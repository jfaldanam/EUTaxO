![Eudaphobase icon](https://www.eudaphobase.eu/wp-content/uploads/eudaphobase-logotype.svg)
# EUdaphobase Taxonomy ontology
[![pipeline status](https://gitlab.com/jfaldanam/EUTaxO/badges/master/pipeline.svg)](https://gitlab.com/jfaldanam/EUTaxO/-/commits/master)
[![Latest Release](https://gitlab.com/jfaldanam/EUTaxO/-/badges/release.svg)](https://gitlab.com/jfaldanam/EUTaxO/-/releases)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7251969.svg)](https://doi.org/10.5281/zenodo.7251969)



This repository includes the [base ontology](EUTaxO.owl), as well as a [python script](parse_api.py) to populate the taxonomy from [the Edaphobase API](https://api.edaphobase.org/).

The populated taxonomy is automatically re-generated the last day of each month, and is available at the following URIs:

| Documentation                                   | Ontology                                               |
| ----------------------------------------------- | ------------------------------------------------------ |
| [https://w3id.org/EUTaxO](https://w3id.org/EUTaxO)                           | [https://jfaldanam.gitlab.io/EUTaxO/EUTaxO.owl](https://jfaldanam.gitlab.io/EUTaxO/EUTaxO.owl)                           |
| [https://w3id.org/EUTaxO/animalia](https://w3id.org/EUTaxO/animalia)                           | [https://jfaldanam.gitlab.io/EUTaxO/animalia/EUTaxO-animalia.owl](https://jfaldanam.gitlab.io/EUTaxO/animalia/EUTaxO-animalia.owl)                           |
| [https://w3id.org/EUTaxO/bacteria](https://w3id.org/EUTaxO/bacteria)                           | [https://jfaldanam.gitlab.io/EUTaxO/bacteria/EUTaxO-bacteria.owl](https://jfaldanam.gitlab.io/EUTaxO/bacteria/EUTaxO-bacteria.owl)                           |
| [https://w3id.org/EUTaxO/chromista](https://w3id.org/EUTaxO/chromista)                           | [https://jfaldanam.gitlab.io/EUTaxO/chromista/EUTaxO-chromista.owl](https://jfaldanam.gitlab.io/EUTaxO/chromista/EUTaxO-chromista.owl)                           |
| [https://w3id.org/EUTaxO/fungi](https://w3id.org/EUTaxO/fungi)                           | [https://jfaldanam.gitlab.io/EUTaxO/fungi/EUTaxO-fungi.owl](https://jfaldanam.gitlab.io/EUTaxO/fungi/EUTaxO-fungi.owl)                           |

# How to generate the ontology instances
To generate the scripts `Python>=3` is required.

1. Install the required dependencies

`pip install -r requirements.txt`

2. Execute the script [parse_api.py](parse_api.py) with any optional parameter if needed

```console
$ python parse_api.py -h
usage: parse_api.py [-h] [-o OUTPUT] [-f FORMAT]

options:
  -h, --help            show this help message and exit
  -o OUTPUT, --output OUTPUT
                        Output folder
  -f FORMAT, --format FORMAT
                        RDF serialization format supported by RDFlib:
                        https://rdflib.readthedocs.io/en/stable/plugin_serializers.html
$ python parse_api.py -o public -f xml
```

# Documentation
Use [`pylode`](https://github.com/RDFLib/pyLODE) to generate the documentation for the main ontology or the generated ones from the EDaphobase API:
```console
$ python -m pylode EUTaxO.owl -o docs.html
$ ls public/ | xargs -I {} -n 1 python -m pylode public/{}/EUTaxO-{}.owl -o public/{}/index.html
```

# Changelogs
> **Warning**: The updates to the actual taxonomy data (EUdaphobase API) is not controlled in this repository, there may be changes on the resulting ontology without being listed here.

## Changelog v3.0.0
This major version splits the ontology in smaller ontologies according to the Kingdoms available at the EDaphobase ontology.
* Replaced "https://w3id.org/EUTaxO/hierarchy" for "https://w3id.org/EUTaxO/<kingdom>", where kingdom is one of ["animalia", "bacteria", "chromista", "fungi"] at the time of this release.

## Changelog v2.1.0
* Removal of Animalia as a fixed class, it is now loaded from Edaphobase.

## Changelog v2.0.0
This major version adapts the ontology to better comply with the FAIR principles.
* Changed URI from "https://www.eudaphobase.eu/ontologies/taxonomy" to "https://w3id.org/EUTaxO"
* Complete ontology metadata
* Complete labels and comments for all terms
* Remove support to parse output excel file
* Remove all spaces from URIs

## Changelog v1.3.0
* Add Animalia taxon above the phylum rank.
* Fix phylum property pointing to kingdom instead of phylum.
* Update phylum and kingdom from data properties to object properties.
* Remove "Unknown" string when there is no known author or year.

## Changelog v1.2.1
* Added kingdom and phylum data properties

## Changelog v1.2.0
* Added ID data property to taxon
* Changed synonym from data to object property
* Changed URI from `https://www.eudaphobase.eu/ontologies/taxonomy-hierarchy` to `https://www.eudaphobase.eu/ontologies/taxonomy`

## Changelog v1.1.0

* Stay with hierarchy version
* Use properties from Darwin ontology
* ScientificName in leaf nodes must be: Genus + SpecificEpithet
* Added versioning to the ontology
* Make leaf nodes be only instances of parent class
* Added acceptedNameUsage
* Make Synonym a object property
